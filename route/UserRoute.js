import express from "express";
import { getUsers, getUsersById, createUser, updateUser, deleteUser } from "../controller/UserController.js";
const router = express.Router();

router.get( '/users', getUsers )
router.get( '/users/:id', getUsersById )
router.post( '/users', createUser )
router.delete( '/users/:id', deleteUser )
router.patch( '/users/:id', updateUser )

export default router;